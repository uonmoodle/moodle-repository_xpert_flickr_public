<?php
// This file is part of the Flickr public (Xpert) repository plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Cachable session options for the repository.
 *
 * @package    repository_xpert_flickr_public
 * @copyright  2020 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace repository_xpert_flickr_public;

/**
 * Cachable session options for the repository.
 *
 * @package    repository_xpert_flickr_public
 * @copyright  2020 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class options implements \cacheable_object {
    /** @var string The tags that images should have. */
    public $tag;

    /** @var string Search text that the images should have. */
    public $text;

    /** @var string The size the image will be scaled to. */
    public $size;

    /** @var string The colour that will be used for the copyright message. */
    public $colours;

    /**
     * {@see \cacheable_object::prepare_to_cache()}
     */
    public function prepare_to_cache() {
        return json_encode($this);
    }

    /**
     * {@see \cacheable_object::wake_from_cache()}
     */
    public static function wake_from_cache($data) {
        $values = json_decode($data);
        $options = new options();
        $options->colours = $values->colours;
        $options->size = $values->size;
        $options->tag = $values->tag;
        $options->text = $values->text;
        return $options;
    }
}
