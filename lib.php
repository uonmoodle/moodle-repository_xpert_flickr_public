<?php
// This file is part of the Flickr public (Xpert) repository plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This plugin is used to access flickr pictures and append copyright information
 *
 * @package    repository_xpert_flickr_public
 * @copyright  2013 onwards, University of Nottingham
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/flickrlib.php');
require_once(__DIR__ . '/image.php');

/**
 * Class used to create xpert_flickr_public repository
 *
 * @package    repository_xpert_flickr_public
 * @copyright  2013 onwards, University of Nottingham
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class repository_xpert_flickr_public extends repository {
    /**
     * phpFlickr object
     * @var phpFlickr
     */
    private $flickr;
    /**
     * Stores sizes of images to prevent multiple API call
     * @var array
     */
    static private $sizes = array();

    /** @var \repository_xpert_flickr_public\loader The session cache for the plugin. */
    protected $cache;

    /**
     * constructor method
     *
     * @global object $CFG
     * @param int $repositoryid
     * @param int $context
     * @param array $options
     * @param boolean $readonly
     */
    public function __construct($repositoryid, $context = SYSCONTEXTID, $options = array(), $readonly=0) {
        parent::__construct($repositoryid, $context, $options, $readonly);

        $this->cache = new \repository_xpert_flickr_public\loader();

        $this->api_key = $this->get_option('api_key');
        $this->flickr  = new phpFlickr($this->api_key);
        $this->usexpertattribution = $this->get_option('useattribution');
        $this->debug = $this->get_option('debug');

        $account  = optional_param('flickr_account', '', PARAM_RAW);
        $fulltext = optional_param('flickr_fulltext', '', PARAM_RAW);
        $size = optional_param('xpert_flickr_public_size', '', PARAM_RAW);
        if (empty($fulltext)) {
            $fulltext = optional_param('s', '', PARAM_RAW);
        }
        $tag      = optional_param('flickr_tag', '', PARAM_RAW);
        $license  = optional_param('flickr_license', '', PARAM_RAW);
        $colours  = optional_param('colours', '', PARAM_TEXT);

        if (!empty($account) || !empty($fulltext) || !empty($tag) || !empty($license)) {
            $options = new \repository_xpert_flickr_public\options();
            $options->tag = $tag;
            $options->text = $fulltext;
            $options->size = $size;
            $options->colours = $colours;
            $this->cache->save_options($this->id, $options);
        }
    }

    /**
     * save api_key in config table
     * @param array $options
     * @return boolean
     */
    public function set_option($options = array()) {
        if (!empty($options['api_key'])) {
            set_config('api_key', trim($options['api_key']), 'xpert_flickr_public');
        }
        if (!empty($options['debug'])) {
            set_config('debug', trim($options['debug']), 'xpert_flickr_public');
        }
        unset($options['api_key']);
        unset($options['debug']);
        return parent::set_option($options);
    }

    /**
     * get api_key from config table
     *
     * @param string $config
     * @return mixed
     */
    public function get_option($config = '') {
        if ($config === 'api_key') {
            return trim(get_config('xpert_flickr_public', 'api_key'));
        }
        if ($config === 'debug') {
            return trim(get_config('xpert_flickr_public', 'debug'));
        }
        return parent::get_option($config);
    }

    /**
     * is global_search available?
     *
     * @return boolean
     */
    public function global_search() {
        return true;
    }

    /**
     * check if flickr account
     * @return boolean
     */
    public function check_login() {
        return false;
    }

    /**
     * construct login form
     *
     * @return array|void
     */
    public function print_login() {
        if ($this->options['ajax']) {
            $ret = array();
            $fulltext = new stdClass();
            $fulltext->label = get_string('fulltext', 'repository_xpert_flickr_public') . ': ';
            $fulltext->id    = 'el_fulltext';
            $fulltext->type = 'text';
            $fulltext->name = 'flickr_fulltext';

            $tag = new stdClass();
            $tag->label = get_string('tag', 'repository_xpert_flickr_public') . ': ';
            $tag->id    = 'el_tag';
            $tag->type = 'text';
            $tag->name = 'flickr_tag';

            $size = new stdClass();
            $size->type = 'select';
            $size->options = array(
                (object)array(
                    'value' => 'standard',
                    'label' => get_string('standard', 'repository_xpert_flickr_public')
                ),
                (object)array(
                    'value' => 'small',
                    'label' => get_string('small', 'repository_xpert_flickr_public')
                ),
                (object)array(
                    'value' => 'medium',
                    'label' => get_string('medium', 'repository_xpert_flickr_public'),
                    'selected' => 'selected'
                ),
                (object)array(
                    'value' => 'large',
                    'label' => get_string('large', 'repository_xpert_flickr_public')
                )
            );
            $size->id = 'xpert_flickr_public_size';
            $size->name = 'xpert_flickr_public_size';
            $size->label = get_string('size', 'repository_xpert_flickr_public') . ': ';

            $colours = new stdClass();
            $colours->label = get_string('colour', 'repository_xpert_flickr_public') . ': ';
            $colours->type = 'radio';
            $colours->id = 'colours';
            $colours->name = 'colours';
            $colours->value = 'colourwhiteblack|colourblackwhite|colourgreyblack|colourbluebrown|colourpeachbrown|colouryellowblack|colourpinkblack';
            $colours->value_label = '<div class="colour colourwhiteblack">'
                    . get_string('colourwhiteblack', 'repository_xpert_flickr_public') . '</div>|' .
                    '<div class="colour colourblackwhite">' .get_string('colourblackwhite', 'repository_xpert_flickr_public') . '</div>|' .
                    '<div class="colour colourgreyblack">' .get_string('colourgreyblack', 'repository_xpert_flickr_public') . '</div>|' .
                    '<div class="colour colourbluebrown">' .get_string('colourbluebrown', 'repository_xpert_flickr_public') . '</div>|' .
                    '<div class="colour colourpeachbrown">' .get_string('colourpeachbrown', 'repository_xpert_flickr_public') . '</div>|' .
                    '<div class="colour colouryellowblack">' .get_string('colouryellowblack', 'repository_xpert_flickr_public') . '</div>|' .
                    '<div class="colour colourpinkblack">' .get_string('colourpinkblack', 'repository_xpert_flickr_public') . '</div>';

            $ret['login'] = array($fulltext, $tag, $size, $colours);
            $ret['login_btn_label'] = get_string('search');
            $ret['login_btn_action'] = 'search';
            return $ret;
        } else {
            $sizes = self::get_sizes();
            echo '<table>';
            // Full text element.
            echo '<tr><td class="mdl-right"><label>'.get_string('fulltext', 'repository_xpert_flickr_public').':</label></td>';
            echo '<td class="mdl-left"><input type="text" name="flickr_fulltext" /></td></tr>';
            // Tag element.
            echo '<tr><td class="mdl-right"><label>'.get_string('tag', 'repository_xpert_flickr_public').':</label></td>';
            echo '<td class="mdl-left"><input type="text" name="flickr_tag" /></td></tr>';
            // Size element.
            echo '<tr><td class="mdl-right"><label>' . get_string('size') . ':</label></td>';
            echo '<td class="mdl-left"><select id="xpert_flickr_public_size" name="xpert_flickr_public_size">';
            foreach ($sizes as $sizekey => $sizename) {
                echo '<option value="' . $sizekey . '">' . $sizename . '</option>';
            }
            echo '</select></td></tr>';
            echo '<tr class="fp-xpert_flickr_public-colour">
                      <td class="mdl-right"><label>' . get_string('colour', 'repository_xpert_flickr_public') . '</label>:</td>
                      <td class="mdl-left">
                      <div class="colourradiobutton"><input type="radio" name="colour" value="colourwhiteblack" checked><div class="colourwhiteblack">' .
                                  get_string('colourwhiteblack', 'repository_xpert_flickr_public'). '</div></div><br/>
                      <div class="colourradiobutton"><input type="radio" name="colour" value="colourblackwhite"><div class="colourblackwhite">' .
                                  get_string('colourblackwhite', 'repository_xpert_flickr_public') . '</div></div><br/>
                      <div class="colourradiobutton"><input type="radio" name="colour" value="colourgreyblack"><div class="colourgreyblack">' .
                                  get_string('colourgreyblack', 'repository_xpert_flickr_public') . '</div></div><br/>
                      <div class="colourradiobutton"><input type="radio" name="colour" value="colourbluebrown"><div class="colourbluebrown">' .
                                  get_string('colourbluebrown', 'repository_xpert_flickr_public') . '</div></div><br/>
                      <div class="colourradiobutton"><input type="radio" name="colour" value="colourpeachbrown"><div class="colourpeachbrown">' .
                                  get_string('colourpeachbrown', 'repository_xpert_flickr_public') . '</div></div><br/>
                      <div class="colourradiobutton"><input type="radio" name="colour value="colouryellowblack"><div class="colouryellowblack">' .
                                  get_string('colouryellowblack', 'repository_xpert_flickr_public') . '</div></div><br/>
                      <div class="colourradiobutton"><input type="radio" name="colour" value="colourpinkblack"><div class="colourpinkblack">' .
                                  get_string('colourpinkblack', 'repository_xpert_flickr_public') . '</div></div><br/>
                      </td></tr>';
            echo '</table>';
            echo '<input type="hidden" name="action" value="search" />';
            echo '<input type="submit" value="'.get_string('search', 'repository').'" />';
        }
    }

    /**
     * Get image size options
     *
     * @return array $sizes
     */
    public static function get_sizes() {
        $sizes = array();
        $sizes['standard'] = get_string('standard', 'repository_xpert_flickr_public');
        $sizes['small'] = get_string('small', 'repository_xpert_flickr_public');
        $sizes['medium'] = get_string('medium', 'repository_xpert_flickr_public');
        $sizes['large'] = get_string('large', 'repository_xpert_flickr_public');

        return $sizes;
    }

    /**
     * Destroy session
     *
     * @return array|void
     */
    public function logout() {
        $this->cache->deletion_options($this->id);
        return $this->print_login();
    }

    /**
     * Return license type to search for
     * @param int $licenseid
     * @return string
     */
    public function license4moodle ($licenseid) {
        $license = array('4' => 'cc');
        return $license[$licenseid];
    }

    /**
     * Search images on flickr
     *
     * @param string $searchtext
     * @param int $page
     * @return array
     */
    public function search($searchtext, $page = 1) {
        $ret = array();
        if (empty($page)) {
            $page = 1;
        }

        $options = $this->cache->get_options($this->id);

        $photos = $this->flickr->photos_search(array(
            'tags' => $options->tag,
            'page' => $page,
            'per_page' => 24,
            'user_id' => null,
            'license' => 4, // Include only CC by license.
            'text' => $options->text,
            'extras' => 'owner_name, date_taken, last_update'
            )
        );
        $ret['total'] = $photos['total'];
        $ret['perpage'] = $photos['perpage'];
        if (empty($photos)) {
            $ret['list'] = array();
            return $ret;
        }
        $this->build_list($photos, $page, $ret);
        $ret['list'] = array_filter($ret['list'], array($this, 'filter'));
        return $ret;
    }

    /**
     * Return an image list
     *
     * @param string $path
     * @param int $page
     * @return array
     */
    public function get_listing($path = '', $page = 1) {
        $photos = $this->flickr->people_getPublicPhotos(null, 'original_format', 24, $page);
        $ret = array();

        return $this->build_list($photos, $page, $ret);
    }

    /**
     * Build an image list
     *
     * @param array $photos
     * @param int $page
     * @param array $ret
     * @return array
     */
    private function build_list($photos, $page, &$ret) {
        $ret['list']  = array();
        $ret['nosearch'] = true;
        $ret['norefresh'] = true;
        $ret['logouttext'] = get_string('backtosearch', 'repository_xpert_flickr_public');
        $ret['pages'] = $photos['pages'];
        if (is_int($page) && $page <= $ret['pages']) {
            $ret['page'] = $page;
        } else {
            $ret['page'] = 1;
        }
        if (!empty($photos['photo'])) {
            foreach ($photos['photo'] as $p) {
                if (empty($p['title'])) {
                    $p['title'] = get_string('notitle', 'repository_flickr');
                }
                if (isset($p['originalformat'])) {
                    $format = $p['originalformat'];
                } else {
                    $format = 'jpg';
                }
                $format = '.' . $format;
                if (substr($p['title'], strlen($p['title']) - strlen($format)) != $format) {
                    // Append file extension.
                    $p['title'] .= $format;
                }
                $date = new DateTime($p['datetaken']);
                $ret['list'][] = array(
                    'title' => $p['title'],
                    'source' => $p['id'],
                    'id' => $p['id'],
                    'thumbnail' => $this->flickr->buildPhotoURL($p, 'Square'),
                    'date' => '',
                    'size' => 'unknown',
                    'url' => 'http://www.flickr.com/photos/'.$p['owner'].'/'.$p['id'],
                    'author' => $p['ownername'],
                    'license' => 'cc',
                    'haslicense' => true,
                    'hasauthor' => true,
                    'datemodified' => $p['lastupdate'],
                    'datecreated' => $date->getTimestamp()
                );
            }
        }
        return $ret;
    }

    /**
     * Print a search form
     *
     * @return string
     */
    public function print_search() {
        $str = '';
        $str .= '<input type="hidden" name="repo_id" value="' . $this->id . '" />';
        $str .= '<input type="hidden" name="ctx_id" value="' . $this->context->id . '" />';
        $str .= '<input type="hidden" name="seekey" value="' . sesskey() . '" />';
        $str .= '<label>'.get_string('fulltext', 'repository_xpert_flickr_public') .
                ': </label><br/><input name="s" value="" /><br/>';
        $str .= '<label>'.get_string('tag', 'repository_xpert_flickr_public') .
                '</label><br /><input type="text" name="flickr_tag" /><br />';
        return $str;
    }

    /**
     * Return photo url by given photo id
     * @param string $photoid
     * @return string
     */
    public function get_link($photoid) {
        $bestsize = $this->get_best_size($photoid);
        if (!isset($bestsize['source'])) {
            throw new repository_exception('cannotdownload', 'repository');
        }
        return $bestsize['source'];
    }

    /**
     * Returns the best size for a photo
     *
     * @param string $photoid the photo identifier
     * @return array of information provided by the API
     */
    protected function get_best_size($photoid) {
        if (!isset(self::$sizes[$photoid])) {
            // Sizes are returned from smallest to greatest.
            self::$sizes[$photoid] = $this->flickr->photos_getSizes($photoid);
        }
        $sizes = self::$sizes[$photoid];
        $bestsize = array();
        if (is_array($sizes)) {
            while ($bestsize = array_pop($sizes)) {
                // Make sure the source is set. Exit the loop if found.
                if (isset($bestsize['source'])) {
                    break;
                }
            }
        }
        return $bestsize;
    }

    /**
     * Get the selected file
     *
     * @global object $CFG
     * @param string $photoid
     * @param string $file
     * @return array
     */
    public function get_file($photoid, $file = '') {
        global $CFG;

        $info = $this->flickr->photos_getInfo($photoid);
        if ($info['owner']['realname']) {
            $author = $info['owner']['realname'];
        } else {
            $author = $info['owner']['username'];
        }

        $options = $this->cache->get_options($this->id);

        $size = $options->size;
        if ($size == 'standard') {
            $size = 'medium';
        }
        // If we can read the original secret, it means that we have access to the original picture.
        if (isset($info['originalsecret'])) {
            $source = $this->flickr->buildPhotoURL($info, $size);
        } else {
            $source = $this->build_photo_url($photoid);
        }

        $path = $this->prepare_file($file);
        $c = new curl();
        $dloptions = [
            'filepath' => $path,
            'timeout' => $CFG->repositorygetfiletimeout,
        ];
        $result = $c->download_one($source, null, $dloptions);
        if ($result !== true) {
            throw new moodle_exception('errorwhiledownload', 'repository', '', $result);
        }

        if (!empty($this->usexpertattribution)) {
            $license = __DIR__ . '/pix/' . $size . '_cc.png';
            $url = $info['urls']['url'][0]["_content"];
            $datestamp = $info['dates']['taken'];
            $img = new repository_xpert_flickr_image($path, $author, $url, $license, $datestamp, $photoid, $size,
                    $this->debug, $options->colours);
            $img->add_attribution();
            $img->saveas($path);
        }
        return array('path' => $path, 'author' => $info['owner']['realname'], 'license' => 'cc');
    }

    /**
     * Add Instance settings input to Moodle form
     * @param \moodleform $mform
     */
    public static function instance_config_form($mform) {
        $mform->addElement('checkbox', 'useattribution', get_string('useattribution', 'repository_xpert_flickr_public'));
        $mform->setDefault('useattribution', 1);
    }

    /**
     * Names of the instance settings
     * @return array
     */
    public static function get_instance_option_names() {
        return array_merge(parent::get_instance_option_names(), array('email_address', 'useattribution'));
    }

    /**
     * Add Plugin settings input to Moodle form
     * @param \moodleform $mform
     * @param string $classname
     */
    public static function type_config_form($mform, $classname = 'repository_xpert_flickr_public') {
        parent::type_config_form($mform, $classname);

        $apikey = get_config('xpert_flickr_public', 'api_key');
        if (empty($apikey)) {
            $apikey = '';
        }
        $strrequired = get_string('required');

        $mform->addElement('text', 'api_key', get_string('apikey', 'repository_xpert_flickr_public'),
                array('value' => $apikey, 'size' => '40'));
        $mform->setType('api_key', PARAM_TEXT);
        $mform->addRule('api_key', $strrequired, 'required', null, 'client');
        $mform->addElement('static', null, '',  get_string('information', 'repository_xpert_flickr_public'));
        $mform->addElement('checkbox', 'debug', get_string('debug', 'repository_xpert_flickr_public'));
        $mform->setType('debug', PARAM_INT);
        $mform->addElement('static', '', '', get_string('debug_description', 'repository_xpert_flickr_public'));
        $mform->setDefault('debug', 0);
    }

    /**
     * Names of the plugin settings
     * @return array
     */
    public static function get_type_option_names() {
        return array('api_key', 'pluginname', 'debug');
    }

    /**
     * Is run when moodle administrator add the plugin
     *
     * @return boolean
     */
    public static function plugin_init() {
        // Here we create a default instance for this type.
        $id = repository::static_function('xpert_flickr_public', 'create', 'xpert_flickr_public', 0, context_system::instance(),
                array('name' => '', 'useattribution' => true, 'debug' => 0), 0);
        if (empty($id)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Define list of supported file types
     * @return array
     */
    public function supported_filetypes() {
        return array('web_image');
    }

    /**
     * Define list of supported return types e.g. FILE_EXTERNAL, FILE_INTERNAL, FILE_REFERENCE
     * @return string
     */
    public function supported_returntypes() {
        return (FILE_INTERNAL);
    }

    /**
     * Return photo url by given photo id
     * @param string $photoid
     * @return string
     */
    private function build_photo_url($photoid) {
        $bestsize = $this->get_best_size($photoid);
        if (!isset($bestsize['source'])) {
            throw new repository_exception('cannotdownload', 'repository');
        }
        return $bestsize['source'];
    }
}
